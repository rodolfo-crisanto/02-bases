import { emailTemplate } from "../../src/js-foundation/01-template";

describe('js-foundation', () => {

  test('emailTemplate should contain a greeting', () => {
    expect(emailTemplate).toContain('Hi, ');
  });

});
